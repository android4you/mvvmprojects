package com.mobiledev.superapp.data.models

class CombinedMoviesResponseModel {


    var popular: PopularResponseModel?= null

    var upcoming: PopularResponseModel? = null

    var toprated: PopularResponseModel? = null

    var nowplaying: PopularResponseModel? = null
}