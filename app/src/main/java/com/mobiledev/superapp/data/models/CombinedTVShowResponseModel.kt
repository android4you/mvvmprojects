package com.mobiledev.superapp.data.models

class CombinedTVShowResponseModel {
    var popular: TVShowResponseModel?= null

    var topRated: TVShowResponseModel? = null

    var onAir: TVShowResponseModel? = null

    var airingToday: TVShowResponseModel? = null
}