package com.mobiledev.superapp.data.models

class PeopleResponseModel {


    var page: Int = 0
    var total_results: Int = 0
    var total_pages: Int = 0
    var results: List<ResultsBean>? = null

    class ResultsBean {

        var popularity: Double = 0.toDouble()
        var id: Int = 0
        var profile_path: String? = null
        var name: String? = null
        var isAdult: Boolean = false
        var known_for: List<KnownForBean>? = null

        class KnownForBean {

            var vote_average: Double = 0.toDouble()
            var vote_count: Int = 0
            var id: Int = 0
            var isVideo: Boolean = false
            var media_type: String? = null
            var title: String? = null
            var popularity: Double = 0.toDouble()
            var poster_path: String? = null
            var original_language: String? = null
            var original_title: String? = null
            var backdrop_path: String? = null
            var isAdult: Boolean = false
            var overview: String? = null
            var release_date: String? = null
            var genre_ids: List<Int>? = null
        }
    }
}
