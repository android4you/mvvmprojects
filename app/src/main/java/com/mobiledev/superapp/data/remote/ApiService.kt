package com.mobiledev.superapp.data.remote

import com.mobiledev.superapp.data.models.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/popular?")
    fun getPopularMovies(@Query("api_key") api_key: String, @Query("page") page : Int): Deferred<Response<PopularResponseModel>>

    @GET("movie/top_rated?")
    fun getTopRatedMovies(@Query("api_key") api_key: String, @Query("page") page : Int): Deferred<Response<PopularResponseModel>>

    @GET("movie/now_playing?")
    fun getNowPlayingMovies(@Query("api_key") api_key: String, @Query("page") page : Int): Deferred<Response<PopularResponseModel>>

    @GET("movie/upcoming?")
    fun getUpcomingMovies(@Query("api_key") api_key: String, @Query("page") page : Int): Deferred<Response<PopularResponseModel>>

    @GET("tv/popular?")
    fun getPopularTVShows(@Query("api_key") api_key: String, @Query("page") page : Int): Deferred<Response<TVShowResponseModel>>

    @GET("tv/top_rated?")
    fun getTopRatedTVShows(@Query("api_key") api_key: String, @Query("page") page : Int): Deferred<Response<TVShowResponseModel>>

    @GET("tv/on_the_air?")
    fun getOnAirTVShows(@Query("api_key") api_key: String, @Query("page") page : Int): Deferred<Response<TVShowResponseModel>>

    @GET("tv/airing_today?")
    fun getAiringTodayTVShows(@Query("api_key") api_key: String, @Query("page") page : Int): Deferred<Response<TVShowResponseModel>>

    @GET("person/popular?")
    fun getpopularPeople(@Query("api_key") api_key: String, @Query("page") page: Int):  Deferred<Response<PeopleResponseModel>>

    @GET("movie/{movie_id}?")
    fun getMovieDetails(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String):Deferred<Response<MovieDetailResponseModel>>

    @GET("person/{person_id}?")
    fun getPerson(@Path("person_id") person_id: String, @Query("api_key") api_key: String): Deferred<Response<PeopleDetailModel>>

    @GET("person/{person_id}/movie_credits?")
    fun getPersonMovies(@Path("person_id") person_id: String, @Query("api_key") api_key: String): Deferred<Response<MovieCreditsModel>>

    @GET("person/{person_id}/tv_credits?")
    fun getPersonTVShows(@Path("person_id") person_id: String, @Query("api_key") api_key: String): Deferred<Response<TvShowsCreditsModel>>

    @GET("movie/{movie_id}/images?")
    fun getMovieImages(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String): Deferred<Response<ImagesModel>>


    @GET("movie/{movie_id}/similar?")
    fun getSimilarMovies(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String):Deferred<Response<PopularResponseModel>>


}