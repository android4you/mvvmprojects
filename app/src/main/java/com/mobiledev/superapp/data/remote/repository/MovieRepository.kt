package com.mobiledev.superapp.data.remote.repository

import android.util.Log
import com.mobiledev.superapp.data.models.*
import com.mobiledev.superapp.data.remote.ApiService
import com.mobiledev.superapp.ui.base.BaseRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepository @Inject constructor(private val apiService: ApiService): BaseRepository(){

    suspend fun getPopularMovies(league: String): APIResponse<PopularResponseModel?>  {
        val apiResponse = apiService.getPopularMovies(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getUpcomingMovies(league: String): APIResponse<PopularResponseModel?>  {
        val apiResponse = apiService.getUpcomingMovies(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getTopRatedMovies(league: String): APIResponse<PopularResponseModel?>  {
        val apiResponse = apiService.getTopRatedMovies(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getNowPlayingMovies(league: String): APIResponse<PopularResponseModel?>  {
        val apiResponse = apiService.getNowPlayingMovies(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getPopularTVShows(league: String): APIResponse<TVShowResponseModel?>  {
        val apiResponse = apiService.getPopularTVShows(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getOnAirTVShows(league: String): APIResponse<TVShowResponseModel?>  {
        val apiResponse = apiService.getOnAirTVShows(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getTopRatedTVShows(league: String): APIResponse<TVShowResponseModel?>  {
        val apiResponse = apiService.getTopRatedTVShows(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getAiringTodayTVShows(league: String): APIResponse<TVShowResponseModel?>  {
        val apiResponse = apiService.getAiringTodayTVShows(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getpopularPeople(league: String): APIResponse<PeopleResponseModel?>  {
        val apiResponse = apiService.getpopularPeople(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getMovieDetails(id : String, apikey : String): APIResponse<MovieDetailResponseModel?>  {
        val apiResponse = apiService.getMovieDetails(id, apikey).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getMovieImages(id : String, apikey : String): APIResponse<ImagesModel?>  {
        val apiResponse = apiService.getMovieImages(id, apikey).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getSimilarMovies(id : String, apikey : String): APIResponse<PopularResponseModel?>  {
        val apiResponse = apiService.getSimilarMovies(id, apikey).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }


}
