package com.mobiledev.superapp.data.remote.repository

import com.mobiledev.superapp.BuildConfig
import com.mobiledev.superapp.data.models.*
import com.mobiledev.superapp.data.remote.ApiService
import com.mobiledev.superapp.ui.base.BaseRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PeopleRepository @Inject constructor(private val apiService: ApiService): BaseRepository() {

    suspend fun getPerson(person_id: String): APIResponse<PeopleDetailModel?> {
        val apiResponse = apiService.getPerson(person_id, BuildConfig.API_KEY).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getPersonMovies(person_id: String): APIResponse<MovieCreditsModel?> {
        val apiResponse = apiService.getPersonMovies(person_id, BuildConfig.API_KEY).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getPersonTVShows(person_id: String): APIResponse<TvShowsCreditsModel?> {
        val apiResponse = apiService.getPersonTVShows(person_id, BuildConfig.API_KEY).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }

    suspend fun getpopularPeople(league: String): APIResponse<PeopleResponseModel?>  {
        val apiResponse = apiService.getpopularPeople(league, 1).await()
        return if (apiResponse.isSuccessful)
            APIResponse.Success(apiResponse.body())
        else {
            APIResponse.Error(apiResponse.message(), apiResponse.code())
        }
    }
}