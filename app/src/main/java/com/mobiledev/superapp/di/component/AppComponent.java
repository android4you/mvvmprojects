package com.mobiledev.superapp.di.component;


import com.mobiledev.superapp.di.module.*;
import com.mobiledev.superapp.system.MyApp;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ActivityBuilderModule.class,
        RepositoryModule.class,
        ViewModelModule.class,
        AdapterModule.class,
        NetworkModule.class})
public interface AppComponent extends AndroidInjector<MyApp> {

@Component.Builder
abstract class Builder extends AndroidInjector.Builder<MyApp> {}
}