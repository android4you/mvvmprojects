package com.mobiledev.superapp.di.module;

import com.mobiledev.superapp.ui.detail.DetailActivity;
import com.mobiledev.superapp.ui.home.HomeFragment;
import com.mobiledev.superapp.ui.people.PeopleFragment;
import com.mobiledev.superapp.ui.home.PopularFragment;
import com.mobiledev.superapp.ui.landing.LandingActivity;
import com.mobiledev.superapp.ui.people.PeopleDetailActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {


    @ContributesAndroidInjector
    abstract LandingActivity landingActivity();

   @ContributesAndroidInjector
    abstract HomeFragment homeFragment();

    @ContributesAndroidInjector
    abstract PopularFragment popularFragment();

    @ContributesAndroidInjector
    abstract PeopleFragment peopleFragment();

    @ContributesAndroidInjector
    abstract DetailActivity detailActivity();

    @ContributesAndroidInjector
    abstract PeopleDetailActivity peopleDetailActivity();
}