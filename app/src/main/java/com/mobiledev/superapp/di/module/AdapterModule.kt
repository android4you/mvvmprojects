package com.mobiledev.superapp.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides

@Module
class AdapterModule {



    @Provides
    fun provideGridLayoutManager(activity: AppCompatActivity): GridLayoutManager {
        return GridLayoutManager(activity, 2)
    }

    @Provides
    fun provideLinearLayoutManager(activity: AppCompatActivity): LinearLayoutManager {
        return LinearLayoutManager(activity)
    }
}