package com.mobiledev.superapp.di.module

import com.mobiledev.superapp.data.remote.ApiService
import com.mobiledev.superapp.data.remote.repository.MovieRepository
import com.mobiledev.superapp.data.remote.repository.PeopleRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule{

    @Provides
    @Singleton
    fun provideMovieRepository(apiService: ApiService): MovieRepository {
        return MovieRepository(apiService)
    }


    @Provides
    @Singleton
    fun providePeopleRepository(apiService: ApiService): PeopleRepository {
        return PeopleRepository(apiService)
    }

}
