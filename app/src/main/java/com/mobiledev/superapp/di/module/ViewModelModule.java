package com.mobiledev.superapp.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.mobiledev.superapp.ui.viewmodel.MovieViewModel;
import com.mobiledev.superapp.ui.viewmodel.PeopleViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MovieViewModel.class)
    @SuppressWarnings("unused")
    abstract ViewModel bindsMovieViewModel(MovieViewModel movieViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(PeopleViewModel.class)
    @SuppressWarnings("unused")
    abstract ViewModel bindsPeopleViewModel(PeopleViewModel peopleViewModel);


    @Binds
    @SuppressWarnings("unused")
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);
}