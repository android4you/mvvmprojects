package com.mobiledev.superapp.extensions

import android.content.res.Resources
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.view.*
import android.widget.*
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.widget.addTextChangedListener
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

/***
 * Visibility
 */

fun View.isVisibile(): Boolean = visibility == View.VISIBLE

fun View.isGone(): Boolean = visibility == View.GONE

fun View.isInvisible(): Boolean = visibility == View.INVISIBLE


fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeGone() {
    visibility = View.GONE
}

fun View.makeInvisible() {
    visibility = View.INVISIBLE
}

/**
 * Button enable & disable
 */
fun Button.disableButton() {
    isEnabled = false
    alpha = 0.3f
}

fun Button.enableButton() {
    isEnabled = true
    alpha = 1.0f
}


fun View.setHeight(height: Int) {
    layoutParams.height = height
    requestLayout()
}

fun View.setWidth(width: Int) {
    layoutParams.width = width
    requestLayout()
}

fun View.setSize(height: Int, width: Int) {
    layoutParams.width = width
    layoutParams.height = height
    requestLayout()
}


fun View.getMinHeight() = ViewCompat.getMinimumHeight(this)

fun View.getDimension(@DimenRes id: Int) = resources.getDimension(id)

fun View.getDimensionPixelOffset(@DimenRes id: Int) = resources.getDimensionPixelOffset(id)

fun View.getDimensionPixelSize(@DimenRes id: Int) = resources.getDimensionPixelSize(id)

fun View.getColor(@ColorRes id: Int) = ContextCompat.getColor(context, id)

fun View.getDrawable(@DrawableRes id: Int) = ContextCompat.getDrawable(context, id)

fun ViewGroup.inflate(@LayoutRes id: Int)
        : View = LayoutInflater.from(context).inflate(id, this, false)

inline fun View.click(crossinline callback: (View) -> Unit)
        = setOnClickListener { callback(it) }

inline fun View.longClick(crossinline callback: (View) -> Boolean)
        = setOnLongClickListener { callback(it) }

fun SwipeRefreshLayout.turnOff() = setOnRefreshListener { isRefreshing = false }

inline fun <reified T> View.findById(id: Int): T where T : View = findViewById(id) as T

val View.dm: DisplayMetrics
    get() = resources.displayMetrics


fun Float.pxToDp(): Int {
    val metrics = Resources.getSystem().displayMetrics
    val dp = this / (metrics.densityDpi / 160f)
    return Math.round(dp)
}

fun Float.dpToPx(): Int {
    val metrics = Resources.getSystem().displayMetrics
    val px = this * (metrics.densityDpi / 160f)
    return Math.round(px)
}

fun Int.pxToDp(): Int {
    val metrics = Resources.getSystem().displayMetrics
    val dp = this / (metrics.densityDpi / 160f)
    return Math.round(dp)
}

fun Int.dpToPx(): Int {
    val metrics = Resources.getSystem().displayMetrics
    val px = this * (metrics.densityDpi / 160f)
    return Math.round(px)
}

fun View.dpToPx(dp: Int): Int {
    return (dp * this.dm.density + 0.5).toInt()
}

fun View.pxToDp(px: Int): Int {
    return (px / this.dm.density + 0.5).toInt()
}



fun View.onClick(f: (View) -> Unit) {
    this.setOnClickListener(f)
}

fun View.onLongClick(f: (View) -> Boolean) {
    this.setOnLongClickListener(f)
}

fun View.onTouchEvent(f: (View, MotionEvent) -> Boolean) {
    this.setOnTouchListener(f)
}

fun View.onKeyEvent(f: (View, Int, KeyEvent) -> Boolean) {
    this.setOnKeyListener(f)
}

fun View.onFocusChange(f: (View, Boolean) -> Unit) {
    this.setOnFocusChangeListener(f)
}

fun CompoundButton.onCheckedChanged(f: (CompoundButton, Boolean) -> Unit) {
    this.setOnCheckedChangeListener(f)
}

fun AdapterView<*>.onItemClick(f: (AdapterView<*>, View, Int, Long) -> Unit) {
    this.setOnItemClickListener(f)
}
//https://github.com/limuyang2/AndroidKotlinUtilCode/tree/master/ktutilcode


fun EditText.onChange(cb: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            cb(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}


