package com.mobiledev.superapp.system;


import com.mobiledev.superapp.di.component.DaggerAppComponent;
import com.mobiledev.superapp.utils.SharedPreferencesMgr;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class MyApp extends DaggerApplication {

    private static MyApp instance;
    private static final String TAG = MyApp.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        SharedPreferencesMgr.init(this, "theme");
    }

    public static synchronized MyApp getInstance() {
        return instance;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }
}