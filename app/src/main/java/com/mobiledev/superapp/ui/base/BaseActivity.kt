package com.mobiledev.superapp.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mobiledev.superapp.R
import com.mobiledev.superapp.utils.SharedPreferencesMgr
import dagger.android.support.DaggerAppCompatActivity

import javax.inject.Inject

abstract class BaseActivity<V : ViewModel, D : ViewDataBinding> : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit   var viewModel: V
    lateinit   var dataBinding: D
    @get:LayoutRes
    abstract var layoutRes: Int

    abstract fun getViewModel(): Class<V>

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        if(SharedPreferencesMgr.getInt("theme", 0) == 1) {
            setTheme(R.style.theme_red);
        }
        else if(SharedPreferencesMgr.getInt("theme", 0) == 2) {
            setTheme(R.style.theme_green);
        }
        else if(SharedPreferencesMgr.getInt("theme", 0) == 3) {
            setTheme(R.style.theme_orange);
        }
        else if(SharedPreferencesMgr.getInt("theme", 0) == 4) {
            setTheme(R.style.theme_blue);
        }
        else if(SharedPreferencesMgr.getInt("theme", 0) == 5) {
            setTheme(R.style.theme_dark);
        }

        dataBinding = DataBindingUtil.setContentView(this, layoutRes)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModel())
    }


}