package com.mobiledev.superapp.ui.base

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
//
//abstract class BaseAdapter(activity: Activity) : RecyclerView.Adapter<BaseViewHolder>() {
//
//    var inflater: LayoutInflater
//        protected set
//
//    init {
//        inflater = LayoutInflater.from(activity)
//    }
//}

abstract class BaseAdapter : RecyclerView.Adapter<BaseViewHolder>() {


    fun <T : ViewDataBinding> getDataBinding(
        inflater: LayoutInflater, @LayoutRes id: Int,
        parent: ViewGroup,
        attachParent: Boolean
    ): T {
        return DataBindingUtil.inflate(inflater, id, parent, attachParent)
    }
}