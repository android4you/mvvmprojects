package com.mobiledev.superapp.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment<V : ViewModel, D : ViewDataBinding>: DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit   var viewModel: V
    lateinit   var dataBinding: D
    @get:LayoutRes
    abstract var layoutRes: Int

    private var mRootView: View? = null

    abstract fun getViewModel(): Class<V>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, layoutRes, container, false);
        mRootView = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModel())
        return mRootView
    }
}