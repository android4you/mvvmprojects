package com.mobiledev.superapp.ui.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.databinding.DataBindingUtil
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import android.view.LayoutInflater
import androidx.databinding.ViewDataBinding




abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(model: Any)

}


