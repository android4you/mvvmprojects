package com.mobiledev.superapp.ui.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel( val baseRepository: BaseRepository): ViewModel(), CoroutineScope {

    private var viewModelJob = Job()
    override val coroutineContext: CoroutineContext
    get() = Dispatchers.Main + viewModelJob

    override fun onCleared() {
        super.onCleared()

        viewModelJob.cancel()
    }
}