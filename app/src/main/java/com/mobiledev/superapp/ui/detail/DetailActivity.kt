package com.mobiledev.superapp.ui.detail

import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.util.Log
import com.mobiledev.superapp.R
import com.mobiledev.superapp.databinding.ActivityDetailBinding
import com.mobiledev.superapp.ui.base.BaseActivity
import com.mobiledev.superapp.ui.viewmodel.MovieViewModel
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.mobiledev.superapp.BuildConfig
import com.mobiledev.superapp.data.models.ImagesModel
import com.mobiledev.superapp.data.models.MovieDetailResponseModel
import com.mobiledev.superapp.data.models.PopularResponseModel
import com.mobiledev.superapp.databinding.ImageRowBinding
import com.mobiledev.superapp.databinding.ListItemRowBinding
import com.mobiledev.superapp.ui.generic.GenericAdapter
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : BaseActivity<MovieViewModel, ActivityDetailBinding>() {

    override fun getViewModel(): Class<MovieViewModel> {
        return MovieViewModel::class.java
    }

    override var layoutRes: Int = R.layout.activity_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Glide
            .with(this)
            .load(BuildConfig.IMAGE_URL_SMALL + intent.getStringExtra("image"))
            .centerCrop()
            .placeholder(R.drawable.ic_tab_one)
            .into(posterImage);

        movieDetails()
        getMoviesImages()
        getSimilarMovies()
    }

    private fun movieDetails(){
        viewModel.getMovieDetails(intent.getIntExtra("id", 0).toString(), BuildConfig.API_KEY).observe(this, Observer {
            setData(it)
        })
    }

    private fun setData(model: MovieDetailResponseModel){
        releaseDateTV.text  = model.release_date
        titleTV.text =  model.title
        overViewTV.text = model.overview
        voteCountTV.text = model.vote_count.toString()
        voteAverageTV.text = model.vote_average.toString()
        originalTitleTV.text =model.original_title
        popularityTV.text = model.popularity.toString()
        budgetTV.text = model.budget.toString()
        revenueTV.text = model.revenue.toString()
        for (genresEntity in model.genres!!) {
            genresTV.text = genresTV.text.toString() + "- " + genresEntity.name                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            + "\n"
        }
        statusTV.text = model.status
        homePageTV.text  = model.homepage

        Glide
            .with(this)
            .load(BuildConfig.IMAGE_URL_SMALL + model.poster_path)
            .centerCrop()
            .placeholder(R.drawable.ic_tab_one)
            .into(backDropImage);

    }


    private fun getMoviesImages(){
        viewModel.getMovieImages(intent.getIntExtra("id", 0).toString(), BuildConfig.API_KEY).observe(this, Observer {
            imageAdapter(it)
        })
    }

    private fun imageAdapter(imageModel: ImagesModel){
        if (imageModel.backdrops != null) {
            images_rv!!.setAdapter(object :
                GenericAdapter<ImagesModel.BackdropsEntity, ImageRowBinding>(this, imageModel.backdrops!! ) {

                override fun getLayoutResId(): Int {
                    return R.layout.image_row
                }

                override fun onBindData(
                    model: ImagesModel.BackdropsEntity,
                    position: Int,
                    dataBinding: ImageRowBinding
                ) {
                    Glide.with(context)
                        .load(BuildConfig.IMAGE_URL_SMALL + model.file_path)
                        .centerCrop()
                        .placeholder(R.drawable.ic_tab_one)
                        .apply(RequestOptions.bitmapTransform( RoundedCorners(16)))
                        .into(dataBinding.imageViewRow);
                }

                override fun onItemClick(model: ImagesModel.BackdropsEntity, position: Int) {
                }

            })
        }
    }


    private fun getSimilarMovies(){
        viewModel.getSimilarMovies(intent.getIntExtra("id", 0).toString(), BuildConfig.API_KEY).observe(this, Observer {
            similarMoviesAdapter(it)
        })
    }


    private fun similarMoviesAdapter(popularResponseModel: PopularResponseModel){
        if (popularResponseModel.results != null) {
            similar_rv!!.setAdapter(object :
                GenericAdapter<PopularResponseModel.PopularEntity, ListItemRowBinding>(this, popularResponseModel.results!! ) {

                override fun getLayoutResId(): Int {
                    return R.layout.list_item_row
                }

                override fun onBindData(
                    model: PopularResponseModel.PopularEntity,
                    position: Int,
                    dataBinding: ListItemRowBinding
                ) {
                    dataBinding.titleTv.text = model.title;
                    Glide
                        .with(context)
                        .load(BuildConfig.IMAGE_URL_SMALL + model.poster_path)
                        .centerCrop()
                        .placeholder(R.drawable.ic_tab_one)
                        .into(dataBinding.imageView);
                }

                override fun onItemClick(model: PopularResponseModel.PopularEntity, position: Int) {
                }

            })
        }
    }
}