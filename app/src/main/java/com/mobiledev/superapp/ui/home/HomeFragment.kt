package com.mobiledev.superapp.ui.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.mobiledev.superapp.BuildConfig
import com.mobiledev.superapp.R
import com.mobiledev.superapp.ui.base.BaseFragment
import com.mobiledev.superapp.ui.viewmodel.MovieViewModel
import com.mobiledev.superapp.databinding.FragmentHomeBinding
import com.mobiledev.superapp.ui.people.PeopleAdapter
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment<MovieViewModel, FragmentHomeBinding>() {

    override fun getViewModel(): Class<MovieViewModel> {
        return MovieViewModel::class.java
    }

    override var layoutRes: Int = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.doWorksInParallelMovies(BuildConfig.API_KEY);
        viewModel.combineMovieResponse.observe(this, Observer {
            popular_movies_rv!!.adapter = MovieAdapter(activity!! , it.popular?.results!!)
            upcoming_movies_rv!!.adapter = MovieAdapter(activity!!, it.upcoming?.results!!)
            toprated_movies_rv!!.adapter = MovieAdapter(activity!!, it.toprated?.results!!)
            nowplaying_movies_rv!!.adapter = MovieAdapter(activity!!, it.nowplaying?.results!!)
        })

        viewModel.doWorksInParallelTVShows(BuildConfig.API_KEY);
        viewModel.combineTVShowsResponse.observe(this, Observer {
            popular_tvshows_rv!!.adapter = TVShowsAdapter(activity!!, it.popular?.results!!)
            toprated_tvshows_rv!!.adapter = TVShowsAdapter(activity!!, it.topRated?.results!!)
            airing_tvshows_rv!!.adapter = TVShowsAdapter(activity!!, it.airingToday?.results!!)
            onair_tvshows_rv!!.adapter = TVShowsAdapter(activity!!, it.onAir?.results!!)
        })

        viewModel.getpopularPeople(BuildConfig.API_KEY).observe(this, Observer {
            people_rv.adapter = PeopleAdapter(activity!!, it)
        })
    }

}