package com.mobiledev.superapp.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.mobiledev.superapp.BuildConfig
import com.mobiledev.superapp.R
import com.mobiledev.superapp.data.models.PopularResponseModel
import com.mobiledev.superapp.data.models.TVShowResponseModel
import com.mobiledev.superapp.databinding.FragmentHomeBinding
import com.mobiledev.superapp.ui.base.BaseFragment
import com.mobiledev.superapp.ui.generic.GenericAdapter
import com.mobiledev.superapp.ui.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.fragment_popular.*
import com.mobiledev.superapp.databinding.ListitemMainBinding
import com.mobiledev.superapp.ui.detail.DetailActivity


class PopularFragment : BaseFragment<MovieViewModel, FragmentHomeBinding>() {

    var typeofData = 0

    override fun getViewModel(): Class<MovieViewModel> {
        return MovieViewModel::class.java
    }

    override var layoutRes: Int = R.layout.fragment_popular

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when(typeofData) {
            0 -> {
                viewModel.getPopularMovies(BuildConfig.API_KEY);
                viewModel.popularMoviesResponse.observe(this, Observer { it ->
                    setAdapter(it!!, null)
                })
            }
            1 -> {
                viewModel.getTopRatedMovies(BuildConfig.API_KEY);
                viewModel.topRatedMoviesResponse.observe(this, Observer { it ->
                    setAdapter(it!!, null)
                })
            }
            2 -> {
                viewModel.getNowPlayingMovies(BuildConfig.API_KEY);
                viewModel.nowPlayingMoviesResponse.observe(this, Observer { it ->
                    setAdapter(it!!,null)
                })
            }
            3 -> {
                viewModel.getUpcomingMovies(BuildConfig.API_KEY);
                viewModel.upcomingMoviesResponse.observe(this, Observer { it ->
                    setAdapter(it!!, null)
                })
            }

            4 -> {
                viewModel.getPopularTVShows(BuildConfig.API_KEY);
                viewModel.popularTVShowsResponse.observe(this, Observer { it ->
                    setAdapter(null,  it!!)
                })
            }
            5 -> {
                viewModel.getTopRatedTVShows(BuildConfig.API_KEY);
                viewModel.topRatedTVShowsResponse.observe(this, Observer { it ->
                    setAdapter(null, it!!)
                })
            }
            6 -> {
                viewModel.getOnAirTVShows(BuildConfig.API_KEY);
                viewModel.onAirTVShowsResponse.observe(this, Observer { it ->
                    setAdapter(null,it!!)
                })
            }
            7 -> {
                viewModel.getAiringTodayTVShows(BuildConfig.API_KEY);
                viewModel.airingTodayTVShowsResponse.observe(this, Observer { it ->
                    setAdapter(null,it!!)
                })
            }
            else -> {
                viewModel.getPopularMovies(BuildConfig.API_KEY);
                viewModel.popularMoviesResponse.observe(this, Observer { it ->
                   setAdapter(it!!, null)
                })
            }
        }
    }

    private fun setAdapter(movieList :List<PopularResponseModel.PopularEntity>?, tvShwList: List<TVShowResponseModel.TVShowEntity>?) {

       if(movieList!=null) {
           listview!!.setAdapter(object :
               GenericAdapter<PopularResponseModel.PopularEntity, ListitemMainBinding>(activity, movieList) {
               override fun getLayoutResId(): Int {
                   return R.layout.listitem_main
               }

               override fun onBindData(
                   model: PopularResponseModel.PopularEntity,
                   position: Int,
                   dataBinding: ListitemMainBinding
               ) {
                   dataBinding.txtName.text = model.title

                   dataBinding.txtOriginalTitle.text = model.original_title
                   dataBinding.txtReleaseTV.text = "Release Date: "+model.release_date
                   if(model.poster_path!=null) {
                       Glide
                           .with(context)
                           .load(BuildConfig.IMAGE_URL_SMALL + model.poster_path)
                           .centerCrop()
                           .placeholder(R.drawable.ic_tab_one)
                           .into(dataBinding.imageView);
                   }
               }

               override fun onItemClick(model: PopularResponseModel.PopularEntity, position: Int) {
                   val intent = Intent(context, DetailActivity::class.java)
                   intent.putExtra("image",model.backdrop_path);
                   intent.putExtra("details", model.overview);
                   intent.putExtra("id", model.id);
                   startActivity(intent)
               }

           })
       }
       else if(tvShwList!=null){

        listview!!.setAdapter(object : GenericAdapter<TVShowResponseModel.TVShowEntity, ListitemMainBinding>(activity, tvShwList) {
            override fun getLayoutResId(): Int {
                return R.layout.listitem_main
            }

            override fun onBindData(
                model: TVShowResponseModel.TVShowEntity,
                position: Int,
                dataBinding: ListitemMainBinding
            ) {
                dataBinding.txtName.text = model.name
                dataBinding.txtOriginalTitle.text = model.original_name
                dataBinding.txtReleaseTV.text = "First Air Date: "+model.first_air_date
                if(model.poster_path!=null) {
                    Glide
                        .with(context)
                        .load(BuildConfig.IMAGE_URL_SMALL + model.poster_path)
                        .centerCrop()
                        .placeholder(R.drawable.ic_tab_one)
                        .into(dataBinding.imageView);
                }
            }

            override fun onItemClick(model: TVShowResponseModel.TVShowEntity, position: Int) {
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("image",model.backdrop_path);
                intent.putExtra("details", model.overview);
                intent.putExtra("id", model.id);
                startActivity(intent)
            }
        })
       }

    }
}