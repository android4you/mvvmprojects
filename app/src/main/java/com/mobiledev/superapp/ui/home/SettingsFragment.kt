package com.mobiledev.superapp.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mobiledev.superapp.R
import com.mobiledev.superapp.ui.landing.LandingActivity
import com.mobiledev.superapp.utils.SharedPreferencesMgr
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        red_button.setOnClickListener {
                SharedPreferencesMgr.setInt("theme", 1);
            restartApp()

        }

        green_button.setOnClickListener {
            SharedPreferencesMgr.setInt("theme", 2);
            restartApp()
        }

        orange_button.setOnClickListener {
            SharedPreferencesMgr.setInt("theme", 3);
            restartApp()
        }


        blue_button.setOnClickListener {
            SharedPreferencesMgr.setInt("theme", 4);
            restartApp()
        }
        if(SharedPreferencesMgr.getInt("theme", 5)==5){
            switch_btn_bnw.isChecked = true
        }

        switch_btn_bnw.setOnCheckedChangeListener { buttonView, isChecked ->
             if (isChecked) {
                SharedPreferencesMgr.setInt("theme", 5);
                restartApp()
            } else {
                SharedPreferencesMgr.setInt("theme", 1);
                restartApp()
            }
        }


    }
    fun restartApp(){
        val i = Intent(activity, LandingActivity::class.java)
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i)
    }

}
