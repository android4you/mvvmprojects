package com.mobiledev.superapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mobiledev.superapp.R
import kotlinx.android.synthetic.main.fragment_host.*

class TVShowFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tvshows, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val fragment = PopularFragment()
        replaceFragment(fragment)
        fragment.typeofData = 4
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.popular -> {
                val fragment = PopularFragment()
                replaceFragment(fragment)
                fragment.typeofData = 4
                return@OnNavigationItemSelectedListener true
            }
            R.id.toprated -> {
                val fragment = PopularFragment()
                replaceFragment(fragment)
                fragment.typeofData = 5
                return@OnNavigationItemSelectedListener true
            }
            R.id.onair -> {
                val fragment = PopularFragment()
                replaceFragment(fragment)
                fragment.typeofData = 6
                return@OnNavigationItemSelectedListener true
            }
            R.id.airing -> {
                val fragment = PopularFragment()
                replaceFragment(fragment)
                fragment.typeofData = 7
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun replaceFragment(fragment: Fragment) {
        childFragmentManager
            .beginTransaction()
            .replace(R.id.content, fragment, fragment.javaClass.getSimpleName())
            .commit()
    }
}