package com.mobiledev.superapp.ui.home

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobiledev.superapp.BuildConfig
import com.mobiledev.superapp.R
import com.mobiledev.superapp.data.models.TVShowResponseModel

class TVShowsAdapter(private var context: Activity, private var dataList: List<TVShowResponseModel.TVShowEntity>) :
    RecyclerView.Adapter<TVShowsAdapter.ViewHolder>() {
    override fun getItemCount(): Int {
        return dataList.size;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_view, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = dataList.get(position).name;
        Glide
            .with(context)
            .load(BuildConfig.IMAGE_URL_SMALL + dataList.get(position).poster_path)
            .centerCrop()
            .placeholder(R.drawable.ic_tab_one)
            .into(holder.img);

    }


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var textView: TextView = itemView!!.findViewById(R.id.title_tv)
        var img: ImageView = itemView!!.findViewById(R.id.imageView)
    }
}