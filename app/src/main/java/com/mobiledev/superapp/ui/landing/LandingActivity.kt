package com.mobiledev.superapp.ui.landing

import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.mobiledev.superapp.R
import com.mobiledev.superapp.databinding.ActivityLandingBinding
import com.mobiledev.superapp.ui.base.BaseActivity
import com.mobiledev.superapp.ui.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.activity_landing.*


class LandingActivity : BaseActivity<MovieViewModel, ActivityLandingBinding>() {
    
    override fun getViewModel(): Class<MovieViewModel> {
        return MovieViewModel::class.java
    }

    override var layoutRes: Int = R.layout.activity_landing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val navController = Navigation.findNavController(this, R.id.my_nav_host_fragment)
        setSupportActionBar(toolbar)
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        navigationView.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
            Navigation.findNavController(this, R.id.my_nav_host_fragment),drawerLayout)
    }




}