package com.mobiledev.superapp.ui.people


import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mobiledev.superapp.BuildConfig
import com.mobiledev.superapp.R
import com.mobiledev.superapp.data.models.PeopleResponseModel

class PeopleAdapter(private var context: Activity, private var dataList: List<PeopleResponseModel.ResultsBean>) :
    RecyclerView.Adapter<PeopleAdapter.ViewHolder>() {
    override fun getItemCount(): Int {
        return dataList.size;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.people_row,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtName.text = dataList.get(position).name

        Glide
            .with(context)
            .load(BuildConfig.IMAGE_URL_SMALL + dataList.get(position).profile_path)
            .centerCrop()
            .placeholder(R.drawable.ic_tab_one)
            .apply(RequestOptions.circleCropTransform())
            .into(holder.img);
    }


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        var img: ImageView = itemView!!.findViewById(R.id.people_imageview)
        var txtName : TextView = itemView!!.findViewById(R.id.txtName)
    }
}