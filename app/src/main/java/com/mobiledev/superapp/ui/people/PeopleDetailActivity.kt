package com.mobiledev.superapp.ui.people

import android.os.Bundle
import com.mobiledev.superapp.R
import com.mobiledev.superapp.databinding.ActivityPeopleBinding
import com.mobiledev.superapp.ui.base.BaseActivity
import com.mobiledev.superapp.ui.viewmodel.MovieViewModel

class PeopleDetailActivity :  BaseActivity<MovieViewModel, ActivityPeopleBinding>() {

    override fun getViewModel(): Class<MovieViewModel> {
        return MovieViewModel::class.java
    }

    override var layoutRes: Int = R.layout.activity_people

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}