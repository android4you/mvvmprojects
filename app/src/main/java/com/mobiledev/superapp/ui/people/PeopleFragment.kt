package com.mobiledev.superapp.ui.people

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mobiledev.superapp.BuildConfig
import com.mobiledev.superapp.R
import com.mobiledev.superapp.data.models.PeopleResponseModel
import com.mobiledev.superapp.databinding.FragmentHomeBinding
import com.mobiledev.superapp.databinding.ListPeopleRowBinding
import com.mobiledev.superapp.ui.base.BaseFragment
import com.mobiledev.superapp.ui.generic.GenericAdapter
import com.mobiledev.superapp.ui.viewmodel.PeopleViewModel
import kotlinx.android.synthetic.main.fragment_people.*

class PeopleFragment:  BaseFragment<PeopleViewModel, FragmentHomeBinding>() {

    override fun getViewModel(): Class<PeopleViewModel> {
        return PeopleViewModel::class.java
    }

    override var layoutRes: Int = R.layout.fragment_people

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getpopularPeople(BuildConfig.API_KEY).observe(this, Observer {
            setAdapter(it!!)
        })
    }

    private fun setAdapter(peopleList :List<PeopleResponseModel.ResultsBean>?) {

        if (peopleList != null) {
            listview!!.setAdapter(object :
                GenericAdapter<PeopleResponseModel.ResultsBean, ListPeopleRowBinding>(activity, peopleList) {
                override fun getLayoutResId(): Int {
                    return R.layout.list_people_row
                }

                override fun onBindData(
                    model: PeopleResponseModel.ResultsBean,
                    position: Int,
                    dataBinding: ListPeopleRowBinding
                ) {
                    dataBinding.txtName.text = model.name
                    Glide
                        .with(context)
                        .load(BuildConfig.IMAGE_URL_SMALL + model.profile_path)
                        .centerCrop()
                        .placeholder(R.drawable.ic_tab_one)
                        .apply(RequestOptions.circleCropTransform())
                        .into(dataBinding.imageView);
                }

                override fun onItemClick(model: PeopleResponseModel.ResultsBean, position: Int) {
                    val intent = Intent(context, PeopleDetailActivity::class.java)
                    startActivity(intent)
                }

            })
        }
    }

 }
