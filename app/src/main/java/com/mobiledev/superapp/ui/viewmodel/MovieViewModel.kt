package com.mobiledev.superapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import com.mobiledev.superapp.data.models.*
import com.mobiledev.superapp.data.remote.repository.MovieRepository
import com.mobiledev.superapp.data.remote.repository.PeopleRepository
import com.mobiledev.superapp.ui.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieViewModel @Inject constructor(private val movieRepository: MovieRepository, private val peopleRepository: PeopleRepository) :
    BaseViewModel(movieRepository) {

    var popularMoviesResponse: MutableLiveData<List<PopularResponseModel.PopularEntity>> = MutableLiveData()
    var upcomingMoviesResponse: MutableLiveData<List<PopularResponseModel.PopularEntity>> = MutableLiveData()
    var nowPlayingMoviesResponse: MutableLiveData<List<PopularResponseModel.PopularEntity>> = MutableLiveData()
    var topRatedMoviesResponse: MutableLiveData<List<PopularResponseModel.PopularEntity>> = MutableLiveData()

    var popularTVShowsResponse: MutableLiveData<List<TVShowResponseModel.TVShowEntity>> = MutableLiveData()
    var onAirTVShowsResponse: MutableLiveData<List<TVShowResponseModel.TVShowEntity>> = MutableLiveData()
    var airingTodayTVShowsResponse: MutableLiveData<List<TVShowResponseModel.TVShowEntity>> = MutableLiveData()
    var topRatedTVShowsResponse: MutableLiveData<List<TVShowResponseModel.TVShowEntity>> = MutableLiveData()

    var combineMovieResponse: MutableLiveData<CombinedMoviesResponseModel> = MutableLiveData()
    var combineTVShowsResponse: MutableLiveData<CombinedTVShowResponseModel> = MutableLiveData()

    var peopleResponse :  MutableLiveData<List<PeopleResponseModel.ResultsBean>> = MutableLiveData()

    var movieDetailResponse: MutableLiveData<MovieDetailResponseModel> = MutableLiveData()

    var imagesMovieResponse: MutableLiveData<ImagesModel> = MutableLiveData()


    var similiarMoviesResponse: MutableLiveData<PopularResponseModel> = MutableLiveData()

    fun getPopularMovies(apiKey: String) {
        launch {
            val response = movieRepository.getPopularMovies(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    popularMoviesResponse.postValue(response.body?.results)
                }
                is APIResponse.Error -> {
                    popularMoviesResponse.postValue(null)
                }
            }
        }
    }

    fun getTopRatedMovies(apiKey: String) {
        launch {
            val response = movieRepository.getTopRatedMovies(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    topRatedMoviesResponse.postValue(response.body?.results)
                }
                is APIResponse.Error -> {
                    topRatedMoviesResponse.postValue(null)
                }
            }
        }
    }

    fun getNowPlayingMovies(apiKey: String) {
        launch {
            val response = movieRepository.getNowPlayingMovies(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    nowPlayingMoviesResponse.postValue(response.body?.results)
                }
                is APIResponse.Error -> {
                    nowPlayingMoviesResponse.postValue(null)
                }
            }
        }
    }

    fun getUpcomingMovies(apiKey: String) {
        launch {
            val response = movieRepository.getUpcomingMovies(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    upcomingMoviesResponse.postValue(response.body?.results)
                }
                is APIResponse.Error -> {
                    upcomingMoviesResponse.postValue(null)
                }
            }
        }
    }

    fun getPopularTVShows(apiKey: String) {
        launch {
            val response = movieRepository.getPopularTVShows(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    popularTVShowsResponse.postValue(response.body?.results)
                }
                is APIResponse.Error -> {
                    popularTVShowsResponse.postValue(null)
                }
            }
        }
    }

    fun getTopRatedTVShows(apiKey: String) {
        launch {
            val response = movieRepository.getTopRatedTVShows(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    topRatedTVShowsResponse.postValue(response.body?.results)
                }
                is APIResponse.Error -> {
                    topRatedTVShowsResponse.postValue(null)
                }
            }
        }
    }

    fun getAiringTodayTVShows(apiKey: String) {
        launch {
            val response = movieRepository.getAiringTodayTVShows(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    airingTodayTVShowsResponse.postValue(response.body?.results)
                }
                is APIResponse.Error -> {
                    airingTodayTVShowsResponse.postValue(null)
                }
            }
        }
    }
        fun getOnAirTVShows(apiKey: String) {
            launch {
                val response = movieRepository.getOnAirTVShows(apiKey)
                when (response) {
                    is APIResponse.Success -> {
                        onAirTVShowsResponse.postValue(response.body?.results)
                    }
                    is APIResponse.Error -> {
                        onAirTVShowsResponse.postValue(null)
                    }
                }
            }
        }

        fun doWorksInParallelMovies(teamId: String) {
            var combine = CombinedMoviesResponseModel();
            val popular = async {
                movieRepository.getPopularMovies(teamId)
            }
            val upcoming = async {
                movieRepository.getUpcomingMovies(teamId)
            }

            val toprated = async {
                movieRepository.getTopRatedMovies(teamId)
            }
            val nowplaying = async {
                movieRepository.getNowPlayingMovies(teamId)
            }
            launch {
                val popularresp = popular.await()

                when (popularresp) {
                    is APIResponse.Success -> {
                        combine.popular = popularresp.body
                    }
                    is APIResponse.Error -> {
                    }
                }
                val upComingresp = upcoming.await()
                when (upComingresp) {
                    is APIResponse.Success -> {
                        combine.upcoming = upComingresp.body
                    }
                    is APIResponse.Error -> {
                    }
                }
                val topratedresp = toprated.await()
                when (topratedresp) {
                    is APIResponse.Success -> {
                        combine.toprated = topratedresp.body
                    }
                    is APIResponse.Error -> {
                    }
                }
                val nowPlayingresp = nowplaying.await()
                when (nowPlayingresp) {
                    is APIResponse.Success -> {
                        combine.nowplaying = nowPlayingresp.body
                    }
                    is APIResponse.Error -> {
                    }
                }
                combineMovieResponse.postValue(combine)
                /* println("Kotlin Combined : " + combined) */
            }
        }


        fun doWorksInParallelTVShows(teamId: String) {
            var combine = CombinedTVShowResponseModel();
            val popular = async {
                movieRepository.getPopularTVShows(teamId)
            }
            val airingToday = async {
                movieRepository.getAiringTodayTVShows(teamId)
            }

            val toprated = async {
                movieRepository.getTopRatedTVShows(teamId)
            }
            val onAir = async {
                movieRepository.getOnAirTVShows(teamId)
            }
            launch {
                val popularresp = popular.await()

                when (popularresp) {
                    is APIResponse.Success -> {
                        combine.popular = popularresp.body
                    }
                    is APIResponse.Error -> {
                    }
                }
                val airingTodayresp = airingToday.await()
                when (airingTodayresp) {
                    is APIResponse.Success -> {
                        combine.airingToday = airingTodayresp.body
                    }
                    is APIResponse.Error -> {
                    }
                }
                val topratedresp = toprated.await()
                when (topratedresp) {
                    is APIResponse.Success -> {
                        combine.topRated = topratedresp.body
                    }
                    is APIResponse.Error -> {
                    }
                }
                val onAirresp = onAir.await()
                when (onAirresp) {
                    is APIResponse.Success -> {
                        combine.onAir = onAirresp.body
                    }
                    is APIResponse.Error -> {
                    }
                }
                combineTVShowsResponse.postValue(combine)
                /* println("Kotlin Combined : " + combined) */
            }
        }


        fun getpopularPeople(teamId: String): MutableLiveData<List<PeopleResponseModel.ResultsBean>> {
            CoroutineScope(Dispatchers.IO).launch {
                val response = peopleRepository.getpopularPeople(teamId)
                when (response) {
                    is APIResponse.Success -> {
                        peopleResponse.postValue(response.body?.results)
                    }
                    is APIResponse.Error -> {
                        peopleResponse.postValue(null)
                    }
                }
            }
            return peopleResponse
        }

        fun getMovieDetails(teamId: String, apikey: String): MutableLiveData<MovieDetailResponseModel> {
            CoroutineScope(Dispatchers.IO).launch {
                val response = movieRepository.getMovieDetails(teamId, apikey)
                when (response) {
                    is APIResponse.Success -> {
                        movieDetailResponse.postValue(response.body)
                    }
                    is APIResponse.Error -> {
                        movieDetailResponse.postValue(null)
                    }
                }
            }
            return movieDetailResponse
        }


    fun getMovieImages(teamId: String, apikey: String): MutableLiveData<ImagesModel> {
        CoroutineScope(Dispatchers.IO).launch {
            val response = movieRepository.getMovieImages(teamId, apikey)
            when (response) {
                is APIResponse.Success -> {
                    imagesMovieResponse.postValue(response.body)
                }
                is APIResponse.Error -> {
                    imagesMovieResponse.postValue(null)
                }
            }
        }
        return imagesMovieResponse
    }

    fun getSimilarMovies(teamId: String, apikey: String): MutableLiveData<PopularResponseModel> {
        CoroutineScope(Dispatchers.IO).launch {
            val response = movieRepository.getSimilarMovies(teamId, apikey)
            when (response) {
                is APIResponse.Success -> {
                    similiarMoviesResponse.postValue(response.body)
                }
                is APIResponse.Error -> {
                    similiarMoviesResponse.postValue(null)
                }
            }
        }
        return similiarMoviesResponse
    }



    }
