package com.mobiledev.superapp.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.mobiledev.superapp.data.models.*
import com.mobiledev.superapp.data.remote.repository.PeopleRepository
import com.mobiledev.superapp.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


class PeopleViewModel @Inject constructor(private val peopleRepository: PeopleRepository) :
    BaseViewModel(peopleRepository) {

    var peopleResponse: MutableLiveData<List<PeopleResponseModel.ResultsBean>> = MutableLiveData()
    var peopleDetailResponse: MutableLiveData<PeopleDetailModel> = MutableLiveData()
    var movieCreditResponse: MutableLiveData<MovieCreditsModel> = MutableLiveData()
    var tvShowsCreditResponse: MutableLiveData<TvShowsCreditsModel> = MutableLiveData()


    fun getpopularPeople(apiKey: String) : MutableLiveData<List<PeopleResponseModel.ResultsBean>>{
        launch {
            val response = peopleRepository.getpopularPeople(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    peopleResponse.postValue(response.body?.results)
                }
                is APIResponse.Error -> {
                    peopleResponse.postValue(null)
                    Log.e("error", "error load list matchResponse")
                }
            }
        }
        return peopleResponse
    }

    fun getPerson(apiKey: String) {
        launch {
            val response = peopleRepository.getPerson(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    peopleDetailResponse.postValue(response.body)
                }
                is APIResponse.Error -> {
                    peopleDetailResponse.postValue(null)
                    Log.e("error", "error load list matchResponse")
                }
            }
        }
    }

    fun getPersonMovies(apiKey: String) {
        launch {
            val response = peopleRepository.getPersonMovies(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    movieCreditResponse.postValue(response.body)
                }
                is APIResponse.Error -> {
                    movieCreditResponse.postValue(null)
                    Log.e("error", "error load list matchResponse")
                }
            }
        }
    }

    fun getPersonTVShows(apiKey: String) {
        launch {
            val response = peopleRepository.getPersonTVShows(apiKey)
            when (response) {
                is APIResponse.Success -> {
                    tvShowsCreditResponse.postValue(response.body)
                }
                is APIResponse.Error -> {
                    tvShowsCreditResponse.postValue(null)
                    Log.e("error", "error load list matchResponse")
                }
            }
        }
    }
}