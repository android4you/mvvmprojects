package com.mobiledev.superapp.utils;

import android.content.res.Resources;
import android.view.View;

public interface ColorUiInterface {


    public View getView();

    public void setTheme(Resources.Theme themeId);
}