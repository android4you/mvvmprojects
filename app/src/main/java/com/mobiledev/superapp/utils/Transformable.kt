package com.mobiledev.superapp.utils

interface Transformable {
    fun transformTo();
}